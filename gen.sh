#!/bin/sh

cd data/monad/recipes

# We omit white from this list because white is used as the prototype.
COLORS="orange magenta light_blue yellow lime pink gray light_gray cyan purple blue brown green red black"
for color in $COLORS ; do
    if [ "$INSULATED" = "1" ] ; then
        echo "${color}_insulated_wire.json"
        sed "s/white/$color/g" white_insulated_wire.json > "${color}_insulated_wire.json"
    fi
    if [ "$BUNDLED" = "1" ] ; then
        echo "${color}_bundled_cable.json"
        sed "s/white/$color/g" white_bundled_cable.json > "${color}_bundled_cable.json"
    fi
    if [ "$CLEAN" = "1" ] ; then
        # Delete all generated files.
        rm -f "${color}_insulated_wire.json" "${color}_bundled_cable.json"
    fi
done
